# Change Log

## 2.1.0

- Change: Altered CSS to better account for layouting with PF1 v10.

## 2.0.0.1

- Fix: Bad items directory detection.

## 2.0

- PF1v9 & Foundry v11 compatibility.
- Fix: Item sheet layout problems.
- Fix: Little Helper layout compatibility.

## 1.5.1.2

- Fix: Sheet layout correction was not functioning correctly.

## 1.5.1.1

- Fix: Excessive quantity of scrollbars (especially on Firefox)

## 1.5.1

- New release mechanism for smaller download and install size.

## 1.5.0

- Change: Tooltip is now anchored to bottom of parent element.
  - This enhances compatibility with Little Helper.

## 1.4.1.3 Better handling of uneditable sheets

## 1.4.1.2 Clearing summary properly removes the summary flag

## 1.4.1.1 Layout fix for double scrollbars

## 1.4.1 Compatibility update with PF1 0.80.15

## 1.4.0.2 Fix updating being broken

## 1.4.0.1 Minor v9 compatibility hotfix

## 1.4.0

- Added: Container support.
- Added: Pack summary processing toggle.
- Changed: Pack summary processing disabled by default.
- Fixed: Problems handling numeric summaries.
- Fixed: Item summary was not updated for items directory on update.
- Fixed: Summary editor could sometimes get squished if the sheet was arbitrarily too small.

## 1.3.0

- Fix: Text shadow in summary.
- Fix: Displayed summary was not updated for items directory properly.
- Fix: Item directory summary updates behaved badly.
- Fix: Long summary overflow.
- Removed: Old data format conversion.
- API: Moved from `game.modKbItemSummary` to `game.modules.get(module).api`
- Changed: Debug mode is now enabled via the API.
- Changed: Summary editor is now only on description tab.

## 1.2.1.7 Quick compatibility fix with some other modules

- Summary is now shown only on hovering the item name.  
  Other options need to be considered.

## 1.2.1.6

- Fix: Hover tooltips sometimes being laggy.
- Changed: Tooltip fade is now in pure CSS instead of jquery.
- Changed: Foundry 0.7.x support is dropped, previous versions will continue to work on it fine.

## 1.2.1.5

- Fix: Foundry 0.8.7 stopgap fix for locked compendiums leaking edits.
- Fix: Compendium summaries were erroring and/or not working at all.

## 1.2.1.4 .permission hotfix (as exposed by 1.2.1.3)

## 1.2.1.3 Minor debugging improvement to help hunt some bugs

## 1.2.1.2 Foundry 0.7.x cross-compatibility

## 1.2.1

- Fix: item.collection.apps is undefined
- Changed: Tooltip is now aligned left of pointer if it's past midpoint of screen.

## 1.2

- Minor styling changes.
- Summary is not shown with less than observer permission.
- Foundry VTT 0.8 compatibility.

## 1.1

- New: Tooltips no longer use title="" for display, allowing them to show up faster.
- New: Summary now supports inline rolls. This can be disabled in module settings (client-side).
- Changed: Summary is now stored in item dictionary flags.
  Old summaries will be moved when the summary is updated.
- Fix: Debug output was enabled regardless of setting.

## 1.0.0.1 Hotfix for getFlag errors

## 1.0.0 Dedicated module
