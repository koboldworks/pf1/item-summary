# Item Summary for Pathfinder 1e

Adds ability to add small summary texts to any items (attacks, feats, spells, etc.) that are shown as mouse hover tooltip.

The summary is written in the item edit dialogs:  
![Item Summary in Item](./img/screencaps/summary-item.png)

Which is then shown as mouse hover tooltip on the items.

![Usage](./img/screencaps/usage.png)

## Styling

Style overrides can be achieved in CSS with following:

- `.koboldworks.kbwks-item-summary.tooltip` for the tooltip itself
  - `max-width` can easily control how far the tooltip stretches.
- `.kbwks-stripped-inline-roll` for stripped inline rolls seen in tooltips.

## Known problems

Items Directory items with summaries do not update the displayed value.

## Rationale

Quick description that does not require any clicking.

## Configuration

Not available.

## Compatibility

No known issues.

## Technical

Current format:  
`item.getItemDictionaryFlag('summary')`

Old unsupported format:  
`item.getFlag('koboldworks', 'summary')`

## Install

Manifest URL: <https://gitlab.com/koboldworks/pf1/item-summary/-/releases/permalink/latest/downloads/module.json>

### Old versions

- Foundry 0.8.x: <https://gitlab.com/koboldworks/pf1/item-summary/-/raw/1.4.0.2/module.json>
- Old data format: <https://gitlab.com/koboldworks/pf1/item-summary/-/raw/1.2.1.7/module.json>
- Foundry 0.7.x: <https://gitlab.com/koboldworks/pf1/item-summary/-/raw/1.1/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
