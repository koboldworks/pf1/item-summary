const CFG = {
	id: 'koboldworks-pf1-item-summary',
	SETTINGS: {
		key: 'summary',
		inlineRolls: 'inlineRolls',
		packTooltips: 'packTooltips'
	},
	COLORS: {
		main: 'color:goldenrod',
		number: 'color:mediumpurple',
		unset: 'color:unset'
	},
	debug: false
};

const createNode = (node = 'span', text = '', classes = [], { title = null } = {}) => {
	const n = document.createElement(node);
	n.textContent = text;
	if (classes.length) n.classList.add(...classes);
	if (title) n.title = title;
	return n;
}

const types = ['ActorSheetPFCharacter', 'ActorSheetPFNPC', 'ItemSheetPF'];

const tooltip = () => createNode('span', null, ['koboldworks', 'kbwks-item-summary', 'tooltip']);
const stripTooltip = (dom) => dom.querySelector('.koboldworks.kbwks-item-summary.tooltip')?.remove();

/**
 * @param {Item} item
 * @param {string} summary
 */
async function setSummary(item, summary) {
	return summary.length ? item.setItemDictionaryFlag(CFG.SETTINGS.key, summary) : item.removeItemDictionaryFlag(CFG.SETTINGS.key);
}

/**
 * @param {Item} item
 * @returns {string}
 */
function getSummary(item) {
	if (item instanceof Item) {
		const flag = item.getItemDictionaryFlag(CFG.SETTINGS.key);
		if (flag) return String(flag); // make sure returned value is a string
	}
	else {
		return item.system?.flags?.dictionary?.[CFG.SETTINGS.key];
	}
}

/**
 * @param {ItemSheetPF} sheet
 * @param {JQuery} html
 * @param {object} options
 */
function insertSummaryEditor(sheet, [html], options) {
	try {
		const item = sheet?.object;
		if (!item) return;

		// Update changes
		const _onChangeSummary = async (event) => {
			event.preventDefault();
			event.stopPropagation();
			return setSummary(item, event.target.value?.trim());
		};

		const summary = getSummary(item);
		//  ?? game.i18n.localize('Koboldworks.ItemSummary.NotAvailable')

		// Construct UI.
		const tab = html.querySelector('section .tab[data-tab="description"]');
		tab.classList.add('koboldworks-item-summary-layout-correction');

		const div = document.createElement('div');
		div.classList.add('koboldworks', 'kbwks-item-summary', 'summary-editor', 'form-group');

		const label = document.createElement('label');
		label.classList.add('kbwks-summary-label');
		const icon = document.createElement('i');
		icon.classList.add('fas', 'fa-puzzle-piece');
		label.append(icon, ' ' + game.i18n.localize('Koboldworks.ItemSummary.Editor.Label'));

		const input = document.createElement('input');
		input.value = summary ?? '';
		input.dataset.dtype = 'String';
		input.type = 'text';
		input.placeholder = game.i18n.localize('Koboldworks.ItemSummary.Editor.Hint');
		input.classList.add('kbwks-summary-input');

		div.append(label, input);

		tab.prepend(div);

		// Hook for changes.
		if (sheet.isEditable) input.addEventListener('change', _onChangeSummary);
		else {
			input.readOnly = true;
			input.disabled = true;
		}
	}
	catch (err) {
		console.error('Item Summary | Error:', err);
	}
}

// TODO: Make this appear left of the item for items directory?
/**
 * @param {Element} el
 */
const hookTooltip = (el) => {
	const tt = el.querySelector('.koboldworks.kbwks-item-summary');
	el.addEventListener('mousemove', ev => {
		const ttr = tt.getBoundingClientRect();
		const elr = el.getBoundingClientRect();
		const sw = ttr.width;
		const left = ev.clientX > Math.floor(window.innerWidth / 3 * 2) ? ev.clientX - sw - 14 : ev.clientX + 14;
		const top = elr.bottom + 4; // ev.clientY - 24;
		tt.style.left = `${Math.ceil(left)}px`;
		tt.style.top = `${Math.ceil(top)}px`;
	}, { passive: true });
}

/**
 * @param {Item} item
 * @param {HTMLElement} dom
 */
async function updateItemTooltip(item, dom) {
	if (!item) throw new Error('Item missing');
	if (!dom) throw new Error('DOM Element missing.');
	try {
		const summary = getSummary(item)?.trim();
		if (!summary) return;

		if (!game.user.isGM && !item.testUserPermission?.(game.user, CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER)) return;

		const enrichOrNot = async (t) => game.settings.get(CFG.id, CFG.SETTINGS.inlineRolls) ?
			TextEditor.enrichHTML(t, { rollData: item.getRollData?.() ?? {}, async: true }) : t;

		const tt = tooltip();
		tt.innerHTML = await enrichOrNot(summary);
		tt.querySelectorAll('.inline-roll').forEach(el => {
			el.classList.remove('inline-roll'); // remove clickable box
			el.classList.add('kbwks-stripped-inline-roll');
		});
		tt.querySelectorAll('i.fas.fa-dice-d20').forEach(el => el.remove()); // remove d20 symbols

		stripTooltip(dom);
		dom.classList.add('kbwks-summary');
		dom.append(tt);
		hookTooltip(dom);
	}
	catch (err) {
		console.warn('Item Summary | Failed to update tooltip:', { dom, item }, '\nError:', err.message, err);
	}
}

/**
 * @param {*} dir
 * @param {JQuery} html
 * @param {*} _options
 */
async function itemDirectoryTooltips(dir, [html], _options) {
	html.querySelectorAll('.item')
		.forEach(el => updateItemTooltip(game.items.get(el.dataset.documentId), el));
}

/**
 * @param {Compendium} compendium
 * @param {JQuery<HTMLElement>} html
 */
async function compendiumTooltips(compendium, [html]) {
	if (!game.settings.get(CFG.id, CFG.SETTINGS.packTooltips)) return;

	const cmpData = compendium.collection;
	const type = cmpData.documentName;
	if (type !== 'Item') return;

	const index = await compendium.collection.getIndex({ fields: [`system.flags.dictionary.${CFG.SETTINGS.key}`] });

	const entries = index.filter(ix => ix.system?.flags?.dictionary?.[CFG.SETTINGS.key] !== undefined);
	for (const entry of entries) {
		updateItemTooltip(entry, html.querySelector(`.item[data-document-id="${entry._id}"]`));
	}
}

/**
 *
 * @param {ActorPF|ItemPF} doc
 * @param {JQuery} html
 * @param sheet
 */
function injectSheetItemTooltips(sheet, [html]) {
	const doc = sheet.document;
	html.querySelectorAll('.item-list li.item')
		.forEach((el) => {
			const label = el.querySelector('.item-name');
			if (!label) return; // sanity test
			const item = doc.items.get(el.dataset.itemId);
			if (!item) return; // sanity test
			updateItemTooltip(item, label);
		});
}

/**
 * Update item directory tooltip as the item directory is not re-rendered for updates.
 *
 * @param {Item} item
 * @param {object} changed
 */
function updateItemEvent(item, changed) {
	// If item has parent, we don't care
	if (item.parent) return;

	// Check if summary was updated.
	// BUG: This is likely to break.

	if (changed.system?.flags?.dictionary?.[CFG.SETTINGS.key] === undefined) return;

	const selector = `.item[data-document-id="${item.id}"]`;
	updateItemTooltip(item, ui.items.element[0].querySelector(selector));
}

Hooks.on('renderItemSheetPF', insertSummaryEditor);
Hooks.on('renderActorSheetPF', injectSheetItemTooltips);
Hooks.on('renderItemSheetPF_Container', injectSheetItemTooltips);
Hooks.on('renderItemDirectory', itemDirectoryTooltips);
Hooks.on('renderCompendium', compendiumTooltips);
Hooks.on('updateItem', updateItemEvent);

Hooks.once('init', function initialize() {
	game.modules.get(CFG.id).api = {
		setSummary,
		setDebug: (state) => CFG.debug = Boolean(state),
	}

	game.settings.register(CFG.id, CFG.SETTINGS.inlineRolls, {
		name: 'Koboldworks.ItemSummary.InlineRolls.Label',
		hint: 'Koboldworks.ItemSummary.InlineRolls.Hint',
		scope: 'client',
		type: Boolean,
		default: true,
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.packTooltips, {
		name: 'Koboldworks.ItemSummary.Compendiums.Label',
		hint: 'Koboldworks.ItemSummary.Compendiums.Hint',
		scope: 'client',
		type: Boolean,
		default: false,
		config: true,
	});

	const mod = game.modules.get(CFG.id);
	console.log(`%cItem Summary%c | %c${mod.version}%c | %cReady!`,
		CFG.COLORS.main, CFG.COLORS.unset, CFG.COLORS.number, CFG.COLORS.unset, CFG.COLORS.main);
});
